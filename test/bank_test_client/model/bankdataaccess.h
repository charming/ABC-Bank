#ifndef BANKCLIENTSESSION_H
#define BANKCLIENTSESSION_H
#include <map>
#include <QtNetwork/QTcpSocket>
#include "common/joutstream.h"
using namespace std;

struct RequestHead
{
    unsigned short cmd;
    unsigned short len;
};

struct ResponseHead
{
    unsigned short cmd;
    unsigned short len;
    unsigned short cnt;
    unsigned short seq;
    unsigned short error_code;
    char error_msg[30];
};

struct RequestPack
{
    RequestHead head;
    char buf[1];
};

struct ResponsePack
{
    ResponseHead head;
    char buf[1];
};

class BankDataAccess
{
public:
    BankDataAccess(){}
    virtual ~BankDataAccess(){}
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &,map<QString,QString> &){return false;}
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &,map<QString, QString> &){return false;}
    //这里要提供重置方法，方便控制端进行刷新
    virtual void resetDataAccess(void){}
protected:
    //还应该实现一个包验证的方法
    //在内部各自的实现的响应内部进行调用
    virtual bool checkData();
    virtual void generatePackTailMd5(quint16 seed);
    virtual void encryPassword(QString &passStr, quint16 seed);
    char *data_;
    quint64 dataLen_;
    JOutStream jos_;
};

#endif // BANKCLIENTSESSION_H
