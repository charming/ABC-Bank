#ifndef LOGINDATAACCESS_H
#define LOGINDATAACCESS_H
#include "bankdataaccess.h"

class LoginDataAccess : public BankDataAccess
{
public:
    LoginDataAccess();
    ~LoginDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
};

#endif // LOGINDATAACCESS_H
