#ifndef CHANGEPASSWORDDATAACCESS_H
#define CHANGEPASSWORDDATAACCESS_H
#include "bankdataaccess.h"

class ChangePasswordDataAccess : public BankDataAccess
{
public:
    ChangePasswordDataAccess();
    ~ChangePasswordDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
};

#endif // CHANGEPASSWORDDATAACCESS_H
