#ifndef CLOSEACCOUNTDATAACCESS_H
#define CLOSEACCOUNTDATAACCESS_H
#include "bankdataaccess.h"

class CloseAccountDataAccess : public BankDataAccess
{
public:
    CloseAccountDataAccess();
    ~CloseAccountDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
};

#endif // CLOSEACCOUNTDATAACCESS_H
