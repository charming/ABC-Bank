#include "bankclienttestcase.h"
#include "bankclient.h"
#include "bankexception.h"
#include <QThreadStorage>

QThreadStorage<BankClient> bc;

bool testCaseBalanceInquiry()
{
    try {
        bc.localData().setReqResType("BalanceInquiryDataAccess");
        bc.localData().clear();
        bc.localData().setRawData("id","000003");
        bc.localData().setRawData("pass","123456");
        if(!bc.localData().execDataAccess()) {
            //trans success but func failure
            return false;
        } else {
            //trans success
            return true;
        }
    } catch(BankException &e) {
        //trans failure
        return false;
    }
}
