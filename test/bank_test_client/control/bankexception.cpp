#include "bankexception.h"

const char* BankException::what() const throw()
{
    return message_.c_str();
}

const char* BankException::StackTrace() const throw()
{
    return stackTrace_.c_str();
}

void BankException::FillStackTrace()
{
}

