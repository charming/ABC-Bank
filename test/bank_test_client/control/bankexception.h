#ifndef BANKEXCEPTION_H
#define BANKEXCEPTION_H

#include <exception>
#include <string>

class BankException : public std::exception
{
public:
    explicit BankException(const char* message)
        : message_(message)
    {
        FillStackTrace();
    }

    explicit BankException(const std::string& message)
        : message_(message)
    {
        FillStackTrace();
    }

    virtual ~BankException() throw()
    {

    }

    virtual const char* what() const throw();
    const char* StackTrace() const throw();

private:
    void FillStackTrace();

    std::string message_;
    std::string stackTrace_;
};

#endif // BANKEXCEPTION_H
