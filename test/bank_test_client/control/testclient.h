#ifndef TESTCLIENT_H
#define TESTCLIENT_H
#include <map>
#include <vector>
#include <functional>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QEvent>
#include <QFuture>
#include <QFutureWatcher>
#include <QSharedPointer>
#include <QMutex>
using namespace std;

//建立一个字符串函数的映射
class TestClient : public QObject
{
    Q_OBJECT
    typedef function<bool ()> testFunc;
public:
    TestClient(QObject *parent = 0);
    TestClient(const TestClient&) : QObject(0){}
    ~TestClient(){}
    void testCaseTask();
    void setOneThreadRuntimes(quint64 runtimes)
    {
        runtimes_ = runtimes;
    }
    void setThreadsNum(quint64 cliNum)
    {
        cliNum_ = cliNum;
        finishedCount_ = 0;
    }

    void statisticTestData(QString &msg);
    void runTestCase(QString &name);
    QStringList getTestCaseList()
    {
        return testCaseList_;
    }

signals:
    void sendStatisticMsg(QString msg);
private slots:
    void statisitcFineshedTest();
private:
    map<QString,testFunc> funcMap_;
    vector<QString> testRes_;
    quint64 runtimes_;
    quint64 cliNum_;
    vector<QSharedPointer<QFutureWatcher<void> > > fWatcherVec_;
    quint64 finishedCount_;
    QMutex mutex_;
    //一次运行的时间参数
    vector<int> elapsedTimeVec_;
    QString curTestCaseName_;
    QStringList testCaseList_;
};

#endif // TESTCLIENT_H
