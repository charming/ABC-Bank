#include "testclientmainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TestClientMainWindow w;
    w.show();

    return a.exec();
}
