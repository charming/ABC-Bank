#include <QTableWidgetItem>
#include <QStringList>
#include "testclientmainwindow.h"
#include "ui_testclientmainwindow.h"

TestClientMainWindow::TestClientMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TestClientMainWindow)
{
    ui->setupUi(this);
    //连接客户端和ui信号
    connect(&tc_,SIGNAL(sendStatisticMsg(QString)),
            this,SLOT(getOneThreadStatisticData(QString)));
    //
    QStringList header;
    //时间都是以ms为单位
    header << "总事物量(transactions)" << "有限事物率(availability)"
            << "消耗时间(ms)(elapsed time)" << "响应时间(ms)(response time)"
            << "传输速率(trans rate)" << "并发性(concurrency)"
            << "成功事物量(success transactions)" << "失败事物量(failure transactions)"
            << "最长时间(ms)(max trans time)" << "最短时间(ms)(min trans time)";
    //进行列表的初始设置
    ui->tableWidget->setColumnCount(header.length());
    ui->tableWidget->setHorizontalHeaderLabels(header);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setShowGrid(true);
    //list init
    ui->testCaseComboBox->addItems(tc_.getTestCaseList());
}

TestClientMainWindow::~TestClientMainWindow()
{
    delete ui;
}

void TestClientMainWindow::on_testStartButton_clicked()
{
    //这里开启线程调用客户端进行测试
    int thNum = ui->threadNumSpinBox->text().toInt();
    int reqNum = ui->requestNumSpinBox->text().toInt();
    tc_.setOneThreadRuntimes(reqNum);
    tc_.setThreadsNum(thNum);
    //tc.setTestMessageReceiver(this);
    //这里启动多少个，就跑多少个
    //按照当前列表来一次跑，后期增加功能可以设置一次跑完的功能
    QString testCaseName = ui->testCaseComboBox->currentText();
    tc_.runTestCase(testCaseName);
    ui->testStartButton->setEnabled(false);
}

void TestClientMainWindow::getOneThreadStatisticData(QString s)
{
    ui->testStartButton->setEnabled(true);
    //到时候可以增加一个统计数据保存的功能
    statisticVec_.push_back(s);
    //这里可以进行解析，将数据统计出来
    ui->tableWidget->setRowCount(statisticVec_.size());
    QStringList items,itemVals;
    for(quint32 i = 0;i < statisticVec_.size();++i) {
        //对字符串进行拆分，提取出数据
        items.clear();
        items = statisticVec_[i].split(',');
        for(int j = 0;j < items.length();++j) {
            itemVals.clear();
            itemVals = items[j].split(':');
            QTableWidgetItem *item = \
                new QTableWidgetItem((const char *)(itemVals[1].toLocal8Bit()));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            ui->tableWidget->setItem(i,j,item);
        }
    }
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->show();
}
