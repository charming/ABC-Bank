#ifndef TESTCLIENTMAINWINDOW_H
#define TESTCLIENTMAINWINDOW_H
#include <vector>
#include <QMainWindow>
#include "control/testclient.h"

using namespace std;

namespace Ui {
class TestClientMainWindow;
}

class TestClientMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TestClientMainWindow(QWidget *parent = 0);
    ~TestClientMainWindow();
private slots:
    void on_testStartButton_clicked();
    //注册一个槽，用来接收统计数据
    void getOneThreadStatisticData(QString s);

private:
    Ui::TestClientMainWindow *ui;
    //统计数据vector
    vector<QString> statisticVec_;
    TestClient tc_;
};

#endif // TESTCLIENTMAINWINDOW_H
