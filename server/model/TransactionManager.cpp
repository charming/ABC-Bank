#include <muduo/base/Logging.h>
#include "TransactionManager.h"
#include "UserLogin.h"
#include "OpenAccount.h"
#include "CloseAccount.h"
#include "ChangePassword.h"
#include "Deposit.h"
#include "BalanceInquiry.h"
#include "Transfer.h"
#include "Withdrawal.h"
#include "QueryDayBill.h"
#include "QueryHistoryBill.h"
#include "QueryAccountHistoryBill.h"

using namespace CMD;

TransactionManager::TransactionManager()
{
	m_actions[CMD_LOGIN] = new UserLogin;
	m_actions[CMD_OPEN_ACCOUNT] = new OpenAccount;
	m_actions[CMD_DEPOSIT] = new Deposit;
	m_actions[CMD_WITHDRAW] = new Withdrawal;
	m_actions[CMD_TRANSFER] = new Transfer;
	m_actions[CMD_BALANCE_INQUIRY] = new BalanceInquiry;
	m_actions[CMD_CHANGE_PASSWORD] = new ChangePassword;
	m_actions[CMD_DAY_BILL] = new QueryDayBill;
	m_actions[CMD_HISTORY_BILL] = new QueryHistoryBill;
	m_actions[CMD_ACCOUNT_HISTORY_BILL] = new QueryAccountHistoryBill;
	m_actions[CMD_CLOSE_ACCOUNT] = new CloseAccount;
}

TransactionManager::~TransactionManager()
{
	for (map<uint16_t, Transaction*>::iterator it=m_actions.begin();
		it!=m_actions.end();++it)
	{
			delete it->second;
	}
}

bool TransactionManager::DoAction(BankSession& session)
{
	uint16_t cmd = session.GetCmd();
	if (m_actions.find(cmd) != m_actions.end())
	{
		m_actions[cmd]->Execute(session);
		return true;
	}

	return false;
}