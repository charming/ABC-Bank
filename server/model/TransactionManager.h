#ifndef _TRANSACTION_MANAGER_H_
#define _TRANSACTION_MANAGER_H_
#include <map>
#include <string>
#include <stdint.h>
#include "BankSession.h"
#include "Transaction.h"
using namespace std;
using namespace CMD;

class TransactionManager
{
public:
	TransactionManager();
	~TransactionManager();
	bool DoAction(BankSession& session);
private:
	map<uint16_t, Transaction*> m_actions;
	//TransactionManager(const TransactionManager& rhs);
};

#endif // _TRANSACTION_MANAGER_H_
