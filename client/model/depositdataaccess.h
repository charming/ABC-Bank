#ifndef DEPOSITDATAACCESS_H
#define DEPOSITDATAACCESS_H
#include "bankdataaccess.h"

class DepositDataAccess : public BankDataAccess
{
public:
    DepositDataAccess();
    ~DepositDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
};

#endif // DEPOSITDATAACCESS_H
