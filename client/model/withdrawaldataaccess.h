#ifndef WITHDRAWALDATAACCESS_H
#define WITHDRAWALDATAACCESS_H
#include "bankdataaccess.h"

class WithdrawalDataAccess : public BankDataAccess
{
public:
    WithdrawalDataAccess();
    ~WithdrawalDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
};

#endif // WITHDRAWALDATAACCESS_H
