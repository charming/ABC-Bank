#ifndef SINGLETON_H
#define SINGLETON_H
//实现带范型一个单例模式，把构造和析构都进行私有化处理
template <typename T>
class Singleton
{
public:
    static T &Instance(void)
    {
        static T instance;
        return instance;
    }

private:
    Singleton();
    ~Singleton();
};

#endif // SINGLETON_H
