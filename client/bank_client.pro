#-------------------------------------------------
#
# Project created by QtCreator 2014-07-30T12:06:48
#
#-------------------------------------------------

QT       += core gui network
CONFIG  += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bank_client
TEMPLATE = app


SOURCES += main.cpp\
    view/balanceinquirydialog.cpp \
    view/changepassworddialog.cpp \
    view/closeaccountdialog.cpp \
    view/depositdialog.cpp \
    view/detailstatementdialog.cpp \
    view/logindialog.cpp \
    view/mainmenuwidget.cpp \
    view/openaccountdialog.cpp \
    view/queryaccounthistorybilldialog.cpp \
    view/querydaybilldialog.cpp \
    view/queryhistorybilldialog.cpp \
    view/transferdialog.cpp \
    view/withdrawaldialog.cpp \
    control/bankclient.cpp \
    control/bankexception.cpp \
    common/dynobjectfactory.cpp \
    common/idea.cpp \
    common/jinstream.cpp \
    common/joutstream.cpp \
    common/junit.cpp \
    common/md5.cpp \
    model/balanceinquirydataaccess.cpp \
    model/bankdataaccess.cpp \
    model/changepassworddataaccess.cpp \
    model/closeaccountdataaccess.cpp \
    model/depositdataaccess.cpp \
    model/logindataaccess.cpp \
    model/openaccountdataaccess.cpp \
    model/queryaccounthistorybilldataaccess.cpp \
    model/querydaybilldataaccess.cpp \
    model/queryhistorybilldataaccess.cpp \
    model/transferdataaccess.cpp \
    model/withdrawaldataaccess.cpp \
    control/bankclinetconfig.cpp

HEADERS  += \
    control/singleton.h \
    view/balanceinquirydialog.h \
    view/changepassworddialog.h \
    view/closeaccountdialog.h \
    view/depositdialog.h \
    view/detailstatementdialog.h \
    view/logindialog.h \
    view/mainmenuwidget.h \
    view/openaccountdialog.h \
    view/queryaccounthistorybilldialog.h \
    view/querydaybilldialog.h \
    view/queryhistorybilldialog.h \
    view/transferdialog.h \
    view/withdrawaldialog.h \
    control/bankclient.h \
    control/bankexception.h \
    common/dynobjectfactory.h \
    common/idea.h \
    common/jinstream.h \
    common/joutstream.h \
    common/junit.h \
    common/md5.h \
    common/singleton.h \
    model/balanceinquirydataaccess.h \
    model/bankdataaccess.h \
    model/changepassworddataaccess.h \
    model/closeaccountdataaccess.h \
    model/depositdataaccess.h \
    model/logindataaccess.h \
    model/openaccountdataaccess.h \
    model/queryaccounthistorybilldataaccess.h \
    model/querydaybilldataaccess.h \
    model/queryhistorybilldataaccess.h \
    model/transferdataaccess.h \
    model/withdrawaldataaccess.h \
    model/checkuiinput.h \
    view/checkuiinput.h \
    control/bankclinetconfig.h


FORMS    += mainmenuwidget.ui \
    openaccountdialog.ui \
    depositdialog.ui \
    withdrawaldialog.ui \
    transferdialog.ui \
    balanceinquirydialog.ui \
    changepassworddialog.ui \
    detailstatementdialog.ui \
    closeaccountdialog.ui \
    logindialog.ui \
    querydaybilldialog.ui \
    queryhistorybilldialog.ui \
    queryaccounthistorybilldialog.ui

RESOURCES += \
    img.qrc
