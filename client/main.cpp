#include "view/mainmenuwidget.h"
#include "view/logindialog.h"
#include "control/bankclient.h"
#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //这里客户端当做单例模式初始化
    Singleton<BankClient>::Instance();
    Singleton<BankClinetConfig>::Instance();
    //登陆逻辑实现
    LoginDialog ld;
    ld.show();
    ld.exec();
    if(ld.loginState()) {
        MainMenuWidget w;
        w.show();
        //进行窗口位置调整
        w.move ((QApplication::desktop()->width() - w.width()) / 2,
                (QApplication::desktop()->height() - w.height()) / 2);
        return a.exec();
    } else {
        return 0;
    }
}
