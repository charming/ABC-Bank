#ifndef QUERYDAYBILLDIALOG_H
#define QUERYDAYBILLDIALOG_H

#include <QDialog>

namespace Ui {
class QueryDayBillDialog;
}

class QueryDayBillDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QueryDayBillDialog(QWidget *parent = 0);
    ~QueryDayBillDialog();

private slots:
    void on_queryDayBillButton_clicked();

    void on_queryDayBillPageUpButton_clicked();

    void on_queryDayBillPageDownButton_clicked();

    void on_queryDayBillHomeButton_clicked();

    void on_queryDayBillEndButton_clicked();

private:
    Ui::QueryDayBillDialog *ui;
    qint32 pageInx_;
    QStringList header_;
};

#endif // QUERYDAYBILLDIALOG_H
