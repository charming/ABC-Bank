#include "logindialog.h"
#include "ui_logindialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog),
    loginFlag_(false)
{
    ui->setupUi(this);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

bool
LoginDialog::loginState() const
{
    return loginFlag_;
}

void LoginDialog::on_resetButton_clicked()
{
    ui->userNameLineEdit->clear();
    ui->passwordLineEdit->clear();
}

void LoginDialog::on_loginButton_clicked()
{
    //用户名长度3-10位，字母或者数字
    if(!checkLoginUserName(this,ui->userNameLineEdit->text())) {
        return;
    }
    //密码长度6-8位
    if(!checkLoginUserPassword(this,ui->passwordLineEdit->text())) {
        return;
    }
    try {
        //这里UI操作不涉及到并发，所以单例也就不用加锁。但是如果涉及
        //到并发，那么这里最好就是要加锁，防止资源争用破坏数据。
        BankClient &bc = Singleton<BankClient>::Instance();
        //还需要进行一次清理
        bc.setReqResType("LoginDataAccess");
        bc.clear();
        bc.setRawData("name",ui->userNameLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->passwordLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            //不知为何这里第一次调用消息框的时候打印出来的东西是乱码，
            //返回值正确，第二次同样错误数据返回就正常了。
            //如果是字符串常量，第一次显示就没有问题
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //到这里可以登陆了
            //打印正常的消息
            loginFlag_ = true;
            this->close();
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void LoginDialog::on_loginExitButton_clicked()
{
    this->close();
}
