#ifndef CLOSEACCOUNTDIALOG_H
#define CLOSEACCOUNTDIALOG_H

#include <QDialog>

namespace Ui {
class CloseAccountDialog;
}

class CloseAccountDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CloseAccountDialog(QWidget *parent = 0);
    ~CloseAccountDialog();

private slots:

    void on_closeAccountCommitButton_clicked();

    void on_closeAccountResetButton_clicked();

    void on_closeAccountCloseButton_clicked();

private:
    Ui::CloseAccountDialog *ui;
};

#endif // CLOSEACCOUNTDIALOG_H
