#ifndef WITHDRAWALDIALOG_H
#define WITHDRAWALDIALOG_H

#include <QDialog>

namespace Ui {
class WithdrawalDialog;
}

class WithdrawalDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WithdrawalDialog(QWidget *parent = 0);
    ~WithdrawalDialog();

private slots:

    void on_withdrawalCommitButton_clicked();

    void on_withdrawalResetButton_clicked();

    void on_withdrawalCloseButton_clicked();

private:
    Ui::WithdrawalDialog *ui;
};

#endif // WITHDRAWALDIALOG_H
