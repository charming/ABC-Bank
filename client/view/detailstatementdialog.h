#ifndef DETAILSTATEMENTDIALOG_H
#define DETAILSTATEMENTDIALOG_H

#include <QDialog>
#include <QCloseEvent>

namespace Ui {
class DetailStatementDialog;
}
//使用PIMPL方法来集成窗口
class QueryDayBillDialog;
class QueryAccountHistoryBillDialog;
class QueryHistoryBillDialog;

class DetailStatementDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetailStatementDialog(QWidget *parent = 0);
    ~DetailStatementDialog();

private slots:

    void on_QueryDayBillFormButton_clicked();

    void on_QueryHistoryBillFormButton_clicked();

    void on_QueryAccountHistoryBillFormButton_clicked();

private:
    Ui::DetailStatementDialog *ui;
    QSharedPointer<QueryDayBillDialog> qdbd;
    QSharedPointer<QueryAccountHistoryBillDialog> qahbd;
    QSharedPointer<QueryHistoryBillDialog> qhbd;
    template <typename QueryDialogPointer>
    void CallChildDialog(QueryDialogPointer &qdp);
};

#endif // DETAILSTATEMENTDIALOG_H
