#include "changepassworddialog.h"
#include "ui_changepassworddialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

ChangePasswordDialog::ChangePasswordDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangePasswordDialog)
{
    ui->setupUi(this);
}

ChangePasswordDialog::~ChangePasswordDialog()
{
    delete ui;
}

void ChangePasswordDialog::on_changePasswordCommitButton_clicked()
{
    /*账户ID（文本框）（提示：长度6位，数字）
    账户密码（文本框）（提示：长度6-8位）
    新密码（文本框）（提示：长度6-8位）
    重复密码（文本框）（提示：长度6-8位）
    */
    if(!checkBankAccountId(this,ui->changePasswordIdLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->changePasswordCurrentPasswordLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->changePasswordNewPasswordLineEdit->text())) {
        return;
    }
    if(!checkAccountRepPassword(this,ui->changePasswordNewPasswordLineEdit->text(),
                                ui->changePasswordRepeatPasswordLineEdit->text())) {
        return;
    }
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("ChangePasswordDataAccess");
        bc.clear();
        bc.setRawData("id",ui->changePasswordIdLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->changePasswordCurrentPasswordLineEdit->text().toLocal8Bit());
        bc.setRawData("newPass",ui->changePasswordNewPasswordLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            QString resMsg("密码已修改");
            QMessageBox::information(this,
                tr("修改密码成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void ChangePasswordDialog::on_changePasswordResetButton_clicked()
{
    ui->changePasswordIdLineEdit->clear();
    ui->changePasswordCurrentPasswordLineEdit->clear();
    ui->changePasswordNewPasswordLineEdit->clear();
    ui->changePasswordRepeatPasswordLineEdit->clear();
}

void ChangePasswordDialog::on_changePasswordCloseButton_clicked()
{
    this->close();
}
