#include "transferdialog.h"
#include "ui_transferdialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

TransferDialog::TransferDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TransferDialog)
{
    ui->setupUi(this);
}

TransferDialog::~TransferDialog()
{
    delete ui;
}

void TransferDialog::on_transferCommitButton_clicked()
{
    /*
     *  账户ID（文本框）（提示：长度6位，数字）
        账户密码（文本框）（提示：长度6-8位）
        钱（文本框）（提示：小数最多两位）
        到账户ID（文本框）（提示：长度6位，数字）
    */
    if(!checkBankAccountId(this,ui->transferIdLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->transferPasswordLineEdit->text())) {
        return;
    }
    if(!checkCash(this,ui->transferCashLineEdit->text())) {
        return;
    }
    if(!checkBankAccountId(this,ui->transferToIdLineEdit->text())) {
        return;
    }
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("TransferDataAccess");
        bc.clear();
        bc.setRawData("id",ui->transferIdLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->transferPasswordLineEdit->text().toLocal8Bit());
        bc.setRawData("cash",ui->transferCashLineEdit->text().toLocal8Bit());
        bc.setRawData("toId",ui->transferToIdLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            QString resMsg("交易日期:\t");
            resMsg += bc.getResRawData("resTransDate");
            resMsg += "\n户    名:\t";
            resMsg += bc.getResRawData("resName");
            resMsg += "\n帐    号:\t";
            resMsg += ui->transferIdLineEdit->text();
            resMsg += "\n对方帐号:\t";
            resMsg += ui->transferToIdLineEdit->text();
            resMsg += "\n交易金额:\t";
            resMsg += ui->transferCashLineEdit->text();
            resMsg += "\n摘    要:\t转帐";
            resMsg += "\n余    额:\t";
            resMsg += bc.getResRawData("resBalance");
            QMessageBox::information(this,
                tr("转帐成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void TransferDialog::on_transferResetButton_clicked()
{
    ui->transferCashLineEdit->clear();
    ui->transferIdLineEdit->clear();
    ui->transferPasswordLineEdit->clear();
    ui->transferToIdLineEdit->clear();
}

void TransferDialog::on_transferCloseButton_clicked()
{
    this->close();
}
