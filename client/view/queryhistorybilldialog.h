#ifndef QUERYHISTORYBILLDIALOG_H
#define QUERYHISTORYBILLDIALOG_H

#include <QDialog>

namespace Ui {
class QueryHistoryBillDialog;
}

class QueryHistoryBillDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QueryHistoryBillDialog(QWidget *parent = 0);
    ~QueryHistoryBillDialog();

private slots:
    void on_queryHistoryBillPageUpButton_clicked();

    void on_queryHistoryBillButton_clicked();

    void on_queryHistoryBillPageDownButton_clicked();

    void on_queryHistoryBillHomeButton_clicked();

    void on_queryHistoryBillEndButton_clicked();

private:
    Ui::QueryHistoryBillDialog *ui;
    qint32 pageInx_;
    QStringList header_;
};

#endif // QUERYHISTORYBILLDIALOG_H
