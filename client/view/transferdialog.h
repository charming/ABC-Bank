#ifndef TRANSFERDIALOG_H
#define TRANSFERDIALOG_H

#include <QDialog>

namespace Ui {
class TransferDialog;
}

class TransferDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TransferDialog(QWidget *parent = 0);
    ~TransferDialog();

private slots:

    void on_transferCommitButton_clicked();

    void on_transferResetButton_clicked();

    void on_transferCloseButton_clicked();

private:
    Ui::TransferDialog *ui;
};

#endif // TRANSFERDIALOG_H
