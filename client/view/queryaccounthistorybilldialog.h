#ifndef QUERYACCOUNTHISTORYBILLDIALOG_H
#define QUERYACCOUNTHISTORYBILLDIALOG_H

#include <QDialog>

namespace Ui {
class QueryAccountHistoryBillDialog;
}

class QueryAccountHistoryBillDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QueryAccountHistoryBillDialog(QWidget *parent = 0);
    ~QueryAccountHistoryBillDialog();

private slots:
    void on_queryAccountHistoryBillButton_clicked();

    void on_queryAccountHistoryBillPageUpButton_clicked();

    void on_queryAccountHistoryBillPageDownButton_clicked();

    void on_queryAccountHistoryBillHomeButton_clicked();

    void on_queryAccountHistoryBillEndButton_clicked();

private:
    Ui::QueryAccountHistoryBillDialog *ui;
    qint32 pageInx_;
    QStringList header_;
};

#endif // QUERYACCOUNTHISTORYBILLDIALOG_H
