#include <QDate>
#include <QTableWidgetItem>
#include <QStringList>
#include "querydaybilldialog.h"
#include "ui_querydaybilldialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"
/*
 * 还有几个按键
    pageUp pageDown home end
    然后表可以使用上下键移动标亮当
    前条目，也可以使用鼠标进行点击。
*/
const char *queryDayBillItemHList[] = {
    "resInx" ,
    "resTransactionDate",
    "resOwnerAccountId",
    "resOtherAccountId",
    "resSummary",
    "resTransactionCash",
    "resBalance",
};

QueryDayBillDialog::QueryDayBillDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QueryDayBillDialog),
    pageInx_(0)
{
    ui->setupUi(this);
    //"序号            交易日期     帐号 对方帐号
    //摘要      交易金额          余额"
    header_ << "序号" << "交易日期" << "帐号"
           << "对方帐号" << "摘要" << "交易金额" << "余额";
    //开始如下几个按钮不可用，等第一次查询出结果后才可用
    ui->queryDayBillPageUpButton->setEnabled(false);
    ui->queryDayBillPageDownButton->setEnabled(false);
    ui->queryDayBillHomeButton->setEnabled(false);
    ui->queryDayBillEndButton->setEnabled(false);
}

QueryDayBillDialog::~QueryDayBillDialog()
{
    delete ui;
}

void QueryDayBillDialog::on_queryDayBillButton_clicked()
{
    //注意查询等于是刷新从头开始，将一些数据还原
    pageInx_ = 0;
    //首先是构造查询数据
    QDate qd = ui->queryDayBillDateEdit->date();
    //相当于是对当前页进行刷新
    QString pageInxStr;
    pageInxStr.sprintf("%d",pageInx_);
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("QueryDayBillDataAccess");
        //注意这里的索引也还原
        //当然可能这个缓冲也会丢失，但是为了正确性
        bc.clear();
        bc.setRawData("startDate",qd.toString("yyyy-MM-dd").toLocal8Bit());
        //还应该有个当前页的属性，返回必要的数据
        //当前需要显示的页数
        //关于显示的条目数量，简单期间就设定为固
        //定的大小，这个控件应该可以进行滚动条的添加。
        bc.setRawData("page",pageInxStr.toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //当数据返回后，将数据打印到表上。
            QString curPageItemsNumStr = bc.getResRawData("resItemsPerCurPage");
            qint32 curPageItemsNum = curPageItemsNumStr.toInt();//当前页索引数量
            QString maxPageItemsInxStr = bc.getResRawData("resMaxPageItemsInx");
            qint32 maxPageItemsInx = maxPageItemsInxStr.toInt();//页最大的索引数量
            QString itemStr;
            //直接从map里面取值，首先去除到底返回了多少条目
            //然后条目的存放采用“前缀+索引”的方式进行取回
            //在业务模型处理的地方还要保存总的条目数，已经最
            //大条目上限，用来缓存条目以及健壮性进行控制。
            ui->tableWidget->setColumnCount(header_.length());//列数
            ui->tableWidget->setRowCount(curPageItemsNum);//行数
            ui->tableWidget->setHorizontalHeaderLabels(header_);
            //设置表格编辑模式，这里设置为不可编辑
            ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            //设置表格选择模式，这里设置为整行选择
            ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
            //显示表格线
            ui->tableWidget->setShowGrid(true);
            //按照条目进行插入
            //要根据请求的页计算出当前的序号索引
            for(qint32 i = 0;i < curPageItemsNum;++i) {
                for(quint32 j = 0;j < \
                    sizeof(queryDayBillItemHList) / \
                    sizeof(queryDayBillItemHList[0]);++j) {
                    itemStr.clear();
                    itemStr.sprintf("%s%d",
                        queryDayBillItemHList[j],
                        (i + maxPageItemsInx - curPageItemsNum));
                    QTableWidgetItem *item = \
                            new QTableWidgetItem(bc.getResRawData(itemStr.toLocal8Bit()));
                    //设置为水平和垂直对齐
                    item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                    ui->tableWidget->setItem(i,j,item);
                }
            }
            ui->tableWidget->show();
            //第一次查询结果出来了，这几个按钮已经可用
            ui->queryDayBillPageUpButton->setEnabled(true);
            ui->queryDayBillPageDownButton->setEnabled(true);
            ui->queryDayBillHomeButton->setEnabled(true);
            ui->queryDayBillEndButton->setEnabled(true);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void QueryDayBillDialog::on_queryDayBillPageDownButton_clicked()
{
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        //相当于是增量查询
        pageInx_++;
        QString pageInxStr;
        pageInxStr.sprintf("%d",pageInx_);
        bc.setRawData("page",pageInxStr.toLocal8Bit());
        if(!bc.execDataAccess()) {
            //这里要注意将页数还原
            pageInx_--;
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //当数据返回后，将数据打印到表上。
            QString curPageItemsNumStr = bc.getResRawData("resItemsPerCurPage");
            qint32 curPageItemsNum = curPageItemsNumStr.toInt();
            QString maxPageItemsInxStr = bc.getResRawData("resMaxPageItemsInx");
            qint32 maxPageItemsInx = maxPageItemsInxStr.toInt();
            QString itemStr;
            //直接从map里面取值，首先去除到底返回了多少条目
            //然后条目的存放采用“前缀+索引”的方式进行取回
            //在业务模型处理的地方还要保存总的条目数，已经最
            //大条目上限，用来缓存条目以及健壮性进行控制。
            ui->tableWidget->setColumnCount(header_.length());//列数
            ui->tableWidget->setRowCount(curPageItemsNum);//行数
            ui->tableWidget->setHorizontalHeaderLabels(header_);
            //设置表格编辑模式，这里设置为不可编辑
            ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            //设置表格选择模式，这里设置为整行选择
            ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
            //显示表格线
            ui->tableWidget->setShowGrid(true);
            //按照条目进行插入
            //要根据请求的页计算出当前的序号索引
            for(qint32 i = 0;i < curPageItemsNum;++i) {
                for(quint32 j = 0;j < \
                    sizeof(queryDayBillItemHList) / \
                    sizeof(queryDayBillItemHList[0]);++j) {
                    itemStr.clear();
                    itemStr.sprintf("%s%d",
                        queryDayBillItemHList[j],
                        (i + maxPageItemsInx - curPageItemsNum));
                    QTableWidgetItem *item = \
                            new QTableWidgetItem(bc.getResRawData(itemStr.toLocal8Bit()));
                    //设置为水平和垂直对齐
                    item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                    ui->tableWidget->setItem(i,j,item);
                }
            }
        }
    } catch(BankException &e) {
        //这里要注意将页数还原
        pageInx_--;
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void QueryDayBillDialog::on_queryDayBillPageUpButton_clicked()
{
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        if(pageInx_ > 0) {
            pageInx_--;
        } else {
            pageInx_ = 0;
        }
        QString pageInxStr;
        pageInxStr.sprintf("%d",pageInx_);
        bc.setRawData("page",pageInxStr.toLocal8Bit());
        if(!bc.execDataAccess()) {
            //这里要注意将页数还原
            pageInx_++;
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //当数据返回后，将数据打印到表上。
            QString curPageItemsNumStr = bc.getResRawData("resItemsPerCurPage");
            qint32 curPageItemsNum = curPageItemsNumStr.toInt();
            QString maxPageItemsInxStr = bc.getResRawData("resMaxPageItemsInx");
            qint32 maxPageItemsInx = maxPageItemsInxStr.toInt();
            QString itemStr;
            //直接从map里面取值，首先去除到底返回了多少条目
            //然后条目的存放采用“前缀+索引”的方式进行取回
            //在业务模型处理的地方还要保存总的条目数，已经最
            //大条目上限，用来缓存条目以及健壮性进行控制。
            ui->tableWidget->setColumnCount(header_.length());//列数
            ui->tableWidget->setRowCount(curPageItemsNum);//行数
            ui->tableWidget->setHorizontalHeaderLabels(header_);
            //设置表格编辑模式，这里设置为不可编辑
            ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            //设置表格选择模式，这里设置为整行选择
            ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
            //显示表格线
            ui->tableWidget->setShowGrid(true);
            //按照条目进行插入
            //要根据请求的页计算出当前的序号索引
            for(qint32 i = 0;i < curPageItemsNum;++i) {
                for(quint32 j = 0;j < \
                    sizeof(queryDayBillItemHList) / \
                    sizeof(queryDayBillItemHList[0]);++j) {
                    itemStr.clear();
                    itemStr.sprintf("%s%d",
                        queryDayBillItemHList[j],
                        (i + maxPageItemsInx - curPageItemsNum));
                    QTableWidgetItem *item = \
                            new QTableWidgetItem(bc.getResRawData(itemStr.toLocal8Bit()));
                    //设置为水平和垂直对齐
                    item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                    ui->tableWidget->setItem(i,j,item);
                }
            }
        }
    } catch(BankException &e) {
        //这里要注意将页数还原
        pageInx_++;
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void QueryDayBillDialog::on_queryDayBillHomeButton_clicked()
{
    quint32 pageInxOld_ = pageInx_;
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        pageInx_ = 0;
        QString pageInxStr;
        pageInxStr.sprintf("%d",pageInx_);
        bc.setRawData("page",pageInxStr.toLocal8Bit());
        if(!bc.execDataAccess()) {
            //这里要注意将页数还原
            pageInx_ = pageInxOld_;
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //当数据返回后，将数据打印到表上。
            QString curPageItemsNumStr = bc.getResRawData("resItemsPerCurPage");
            qint32 curPageItemsNum = curPageItemsNumStr.toInt();
            QString maxPageItemsInxStr = bc.getResRawData("resMaxPageItemsInx");
            qint32 maxPageItemsInx = maxPageItemsInxStr.toInt();
            QString itemStr;
            //直接从map里面取值，首先去除到底返回了多少条目
            //然后条目的存放采用“前缀+索引”的方式进行取回
            //在业务模型处理的地方还要保存总的条目数，已经最
            //大条目上限，用来缓存条目以及健壮性进行控制。
            ui->tableWidget->setColumnCount(header_.length());//列数
            ui->tableWidget->setRowCount(curPageItemsNum);//行数
            ui->tableWidget->setHorizontalHeaderLabels(header_);
            //设置表格编辑模式，这里设置为不可编辑
            ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            //设置表格选择模式，这里设置为整行选择
            ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
            //显示表格线
            ui->tableWidget->setShowGrid(true);
            //按照条目进行插入
            //要根据请求的页计算出当前的序号索引
            for(qint32 i = 0;i < curPageItemsNum;++i) {
                for(quint32 j = 0;j < \
                    sizeof(queryDayBillItemHList) / \
                    sizeof(queryDayBillItemHList[0]);++j) {
                    itemStr.clear();
                    itemStr.sprintf("%s%d",
                        queryDayBillItemHList[j],
                        (i + maxPageItemsInx - curPageItemsNum));
                    QTableWidgetItem *item = \
                            new QTableWidgetItem(bc.getResRawData(itemStr.toLocal8Bit()));
                    //设置为水平和垂直对齐
                    item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                    ui->tableWidget->setItem(i,j,item);
                }
            }
        }
    } catch(BankException &e) {
        //这里要注意将页数还原
        pageInx_ = pageInxOld_;
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void QueryDayBillDialog::on_queryDayBillEndButton_clicked()
{
    quint32 pageInxOld_ = pageInx_;
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        QString maxPageInxStr = bc.getResRawData("resMaxPageNum");
        pageInx_ = maxPageInxStr.toInt();
        QString pageInxStr;
        pageInxStr.sprintf("%d",pageInx_);
        bc.setRawData("page",pageInxStr.toLocal8Bit());
        if(!bc.execDataAccess()) {
            //这里要注意将页数还原
            pageInx_ = pageInxOld_;
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //当数据返回后，将数据打印到表上。
            QString curPageItemsNumStr = bc.getResRawData("resItemsPerCurPage");
            qint32 curPageItemsNum = curPageItemsNumStr.toInt();
            QString maxPageItemsInxStr = bc.getResRawData("resMaxPageItemsInx");
            qint32 maxPageItemsInx = maxPageItemsInxStr.toInt();
            QString itemStr;
            //直接从map里面取值，首先去除到底返回了多少条目
            //然后条目的存放采用“前缀+索引”的方式进行取回
            //在业务模型处理的地方还要保存总的条目数，已经最
            //大条目上限，用来缓存条目以及健壮性进行控制。
            ui->tableWidget->setColumnCount(header_.length());//列数
            ui->tableWidget->setRowCount(curPageItemsNum);//行数
            ui->tableWidget->setHorizontalHeaderLabels(header_);
            //设置表格编辑模式，这里设置为不可编辑
            ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            //设置表格选择模式，这里设置为整行选择
            ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
            //显示表格线
            ui->tableWidget->setShowGrid(true);
            //按照条目进行插入
            //要根据请求的页计算出当前的序号索引
            for(qint32 i = 0;i < curPageItemsNum;++i) {
                for(quint32 j = 0;j < \
                    sizeof(queryDayBillItemHList) / \
                    sizeof(queryDayBillItemHList[0]);++j) {
                    itemStr.clear();
                    itemStr.sprintf("%s%d",
                        queryDayBillItemHList[j],
                        (i + maxPageItemsInx - curPageItemsNum));
                    QTableWidgetItem *item = \
                            new QTableWidgetItem(bc.getResRawData(itemStr.toLocal8Bit()));
                    //设置为水平和垂直对齐
                    item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                    ui->tableWidget->setItem(i,j,item);
                }
            }
        }
    } catch(BankException &e) {
        //这里要注意将页数还原
        pageInx_ = pageInxOld_;
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}
