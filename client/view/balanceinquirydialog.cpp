#include "balanceinquirydialog.h"
#include "ui_balanceinquirydialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

BalanceInquiryDialog::BalanceInquiryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BalanceInquiryDialog)
{
    ui->setupUi(this);
}

BalanceInquiryDialog::~BalanceInquiryDialog()
{
    delete ui;
}

void BalanceInquiryDialog::on_balanceInquiryCommitButton_clicked()
{
    /*
     *  账户ID（文本框）（提示：长度6位，数字）
        账户密码（文本框）（提示：长度6-8位）
    */
    if(!checkBankAccountId(this,ui->balanceInquiryIdLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->balanceInquiryPasswordLineEdit->text())) {
        return;
    }
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("BalanceInquiryDataAccess");
        bc.clear();
        bc.setRawData("id",ui->balanceInquiryIdLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->balanceInquiryPasswordLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            QString resMsg("交易日期:\t");
            resMsg += bc.getResRawData("resTransDate");
            resMsg += "\n户    名:\t";
            resMsg += bc.getResRawData("resName");
            resMsg += "\n帐    号:\t";
            resMsg += ui->balanceInquiryIdLineEdit->text();
            resMsg += "\n余    额:\t";
            resMsg += bc.getResRawData("resBalance");
            QMessageBox::information(this,
                tr("余额查询成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void BalanceInquiryDialog::on_balanceInquiryResetButton_clicked()
{
    ui->balanceInquiryIdLineEdit->clear();
    ui->balanceInquiryPasswordLineEdit->clear();
}

void BalanceInquiryDialog::on_balanceInquiryCloseButton_clicked()
{
    this->close();
}
