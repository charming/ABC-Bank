#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    bool loginState() const;
private slots:
    void on_resetButton_clicked();

    void on_loginButton_clicked();

    void on_loginExitButton_clicked();

private:
    Ui::LoginDialog *ui;
    bool loginFlag_;
};

#endif // LOGINDIALOG_H
