
#include "mainmenuwidget.h"
#include "ui_mainmenuwidget.h"
#include "balanceinquirydialog.h"
#include "changepassworddialog.h"
#include "closeaccountdialog.h"
#include "depositdialog.h"
#include "detailstatementdialog.h"
#include "openaccountdialog.h"
#include "transferdialog.h"
#include "withdrawaldialog.h"

MainMenuWidget::MainMenuWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenuWidget),
    bid(new BalanceInquiryDialog),
    cpd(new ChangePasswordDialog),
    cad(new CloseAccountDialog),
    dd(new DepositDialog),
    dsd(new DetailStatementDialog),
    oad(new OpenAccountDialog),
    td(new TransferDialog),
    wd(new WithdrawalDialog)
{
    ui->setupUi(this);
}

MainMenuWidget::~MainMenuWidget()
{
    delete ui;
}

template <typename QChildDialogPointer>
void MainMenuWidget::CallChildDialog(QChildDialogPointer &cd)
{
    this->close();
    //这种写法有效规避了在子
    //窗口内部重新恢复父窗口
    //的做法降低了代码之间的
    //耦合度
    cd->show();
    //这里并非单进程一层层的嵌套下去，
    //如果子窗口再调用子窗口并调用了
    //close之类的退出后，关闭子窗口后
    //就会出现弹出父窗口的情况，说明
    //这里需要进行一些处理。
    cd->exec();//
    this->show();
    //QPoint pos(100,100);
    //this->move(pos);
}

void MainMenuWidget::on_OpenAccountButton_clicked()
{
   CallChildDialog(oad);
}

void MainMenuWidget::on_DepositButton_clicked()
{
    CallChildDialog(dd);
}

void MainMenuWidget::on_WithdrawalButton_clicked()
{
    CallChildDialog(wd);
}

void MainMenuWidget::on_TransferButton_clicked()
{
    CallChildDialog(td);
}

void MainMenuWidget::on_BalanceInquiryButton_clicked()
{
    CallChildDialog(bid);
}

void MainMenuWidget::on_ChangePasswordButton_clicked()
{
    CallChildDialog(cpd);
}

void MainMenuWidget::on_DetailStatementButton_clicked()
{
    CallChildDialog(dsd);
}

void MainMenuWidget::on_CloseAccountButton_clicked()
{
    CallChildDialog(cad);
}

void MainMenuWidget::on_CloseSystemButton_clicked()
{
    QApplication::exit(0);
}
